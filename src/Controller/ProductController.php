<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;

/**
 * @Route("/products", name="product_")
 */

class ProductController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->json([
            'items' => $products
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show($id): Response
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        return $this->json($product);
    }

    /**
     * @Route("/", name="create", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        $data = $request->request->all();

        $product = new Product();
        $product->setName($data['name']);
        $product->setPrice($data['price']);
        $product->setRating($data['rating']);
        $product->setUpdatedAt(new \DateTime('now', new \DateTimezone('Europe/Lisbon')));
       
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($product);
        $doctrine->flush();

        return $this->json([
            'message' => 'Product created!'
        ]);
    }

    /**
     * @Route("/{id}", name="update", methods={"PUT", "PATCH"})
     */
    public function update($id, Request $request): Response
    {
        $data = $request->request->all();

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);;
        
        if ($request->request->has('name'))
            $product->setName($data['name']);

        if ($request->request->has('price'))
            $product->setPrice($data['price']);

        if ($request->request->has('rating'))
            $product->setRating($data['rating']);

        $product->setUpdatedAt(new \DateTime('now', new \DateTimezone('Europe/Lisbon')));
       
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->flush();

        return $this->json([
            'message' => 'Product updated!'
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete($id): Response
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($product);
        $doctrine->flush();

        return $this->json([
            'message' => 'Product deleted!'
        ]);
    }
}
